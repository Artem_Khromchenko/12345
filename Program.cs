﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtemLaba
{
    class Program
    {
        static int id = 0;
        static int id2 = 0;
        static void Main(string[] args)
        {
            Dictionary<int, Book> Telbook = new Dictionary<int, Book>();
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("Это меню программы. Выберите, что вас интересует");
                Console.WriteLine("1--Создание новой записи--");
                Console.WriteLine("2--Редактирование созданных записей--");
                Console.WriteLine("3--Удаление созданных записей--");
                Console.WriteLine("4--Просмотр созданных учетных записей--");
                Console.WriteLine("5--Просмотр всех созданных учетных записей с краткой информацией--");
                Console.WriteLine("6--Выход--");
                int q = int.Parse(Console.ReadLine());
                switch (q)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Создано " + id + " учетных записей. Заполните информацию о человеке:");
                        Console.WriteLine("1.Фамилия; 2.Имя; 3.Отчество; 4.Номер телефона; 5.Страна; 6.Дата рождения; 7.Организация; 8.Должность; 9.Прочие заметки");
                        Telbook[id] = new Book(Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), long.Parse(Console.ReadLine()), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine());
                        id++;
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        id2 = 0;
                        Console.Clear();
                        Console.WriteLine("Вот все ваши контакты. Выберите id контакта для его изменения.");
                        Console.WriteLine(" ");
                        foreach (Book p in Telbook.Values)
                        {
                            Console.WriteLine("Контакт " + id2 + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.familiya);
                            Console.WriteLine("Имя: " + p.imya);
                            Console.WriteLine("Отчество: " + p.otchestvo);
                            Console.WriteLine("Номер телефона: " + p.nomerTel);
                            Console.WriteLine("Страна: " + p.strana);
                            Console.WriteLine("Дата рождения: " + p.Dateofbirthday);
                            Console.WriteLine("Организация: " + p.organizatia);
                            Console.WriteLine("Должность: " + p.dolzhnost);
                            Console.WriteLine("Прочие заметки: " + p.zapisi);
                            Console.WriteLine(" ");
                            id2++;
                        }
                        int z = int.Parse(Console.ReadLine());
                        bool fl = true;
                        while (fl)
                        {
                            Console.WriteLine("1.Изменить фамилию");
                            Console.WriteLine("2.Изменить имя");
                            Console.WriteLine("3.Изменить отчество");
                            Console.WriteLine("4.Изменить номер телефона");
                            Console.WriteLine("5.Изменить страну");
                            Console.WriteLine("6.Изменить дату рождения");
                            Console.WriteLine("7.Изменить организацию");
                            Console.WriteLine("8.Изменить должность");
                            Console.WriteLine("9.Изменить прочие заметки");
                            Console.WriteLine("10.Закончить измения");
                            int sw = int.Parse(Console.ReadLine());
                            switch (sw)
                            {
                                case 1:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую фамилию");
                                        Telbook.ElementAt(z).Value.familiya = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новое имя");
                                        Telbook.ElementAt(z).Value.imya = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новое отчество");
                                        Telbook.ElementAt(z).Value.otchestvo = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новый номер телефона");
                                        Telbook.ElementAt(z).Value.nomerTel = long.Parse(Console.ReadLine());
                                        Console.Clear();
                                        break;
                                    }
                                case 5:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую страну");
                                        Telbook.ElementAt(z).Value.strana = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 6:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую дату рождения");
                                        Telbook.ElementAt(z).Value.Dateofbirthday = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 7:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую организацию");
                                        Telbook.ElementAt(z).Value.organizatia = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 8:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новую должность");
                                        Telbook.ElementAt(z).Value.dolzhnost = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 9:
                                    {
                                        Console.Clear();
                                        Console.WriteLine("Введите новые прочие заметки");
                                        Telbook.ElementAt(z).Value.zapisi = Console.ReadLine();
                                        Console.Clear();
                                        break;
                                    }
                                case 10:
                                    {
                                        Console.WriteLine("Изменения завершены");
                                        fl = false;
                                        break;
                                    }
                            }
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        id2 = 0;
                        Console.Clear();
                        Console.WriteLine("Вот все ваши контакты. Выберите id контакта для его удаления.");
                        Console.WriteLine(" ");
                        foreach (Book p in Telbook.Values)
                        {
                            Console.WriteLine("Контакт " + id2 + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.familiya);
                            Console.WriteLine("Имя: " + p.imya);
                            Console.WriteLine("Отчество: " + p.otchestvo);
                            Console.WriteLine("Номер телефона: " + p.nomerTel);
                            Console.WriteLine("Страна: " + p.strana);
                            Console.WriteLine("Дата рождения: " + p.Dateofbirthday);
                            Console.WriteLine("Организация: " + p.organizatia);
                            Console.WriteLine("Должность: " + p.dolzhnost);
                            Console.WriteLine("Прочие заметки: " + p.zapisi);
                            Console.WriteLine(" ");
                            id2++;
                        }
                        int l = int.Parse(Console.ReadLine());
                        Telbook.Remove(l);
                        Console.WriteLine("Запись удалена.");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        id2 = 0;
                        Console.Clear();
                        Console.WriteLine("Вот список всех ваших контактов в телефонной книге с краткой информацией о каждом:");
                        Console.WriteLine(" ");
                        foreach (Book p in Telbook.Values)
                        {

                            Console.WriteLine("Контакт " + id2 + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.familiya);
                            Console.WriteLine("Имя: " + p.imya);
                            Console.WriteLine("Номер телефона: " + p.nomerTel);
                            Console.WriteLine(" ");
                            id2++;
                        }
                        Console.WriteLine("Выберите, кого хотите посмотреть.");
                        int m = int.Parse(Console.ReadLine());
                        Console.Clear();
                        Console.WriteLine("Контакт " + m + " в телефонной книге");
                        Console.WriteLine(" ");
                        Console.WriteLine("Фамилия: " + Telbook.ElementAt(m).Value.familiya);
                        Console.WriteLine("Имя: " + Telbook.ElementAt(m).Value.imya);
                        Console.WriteLine("Отчество: " + Telbook.ElementAt(m).Value.otchestvo);
                        Console.WriteLine("Номер телефона: " + Telbook.ElementAt(m).Value.nomerTel);
                        Console.WriteLine("Страна: " + Telbook.ElementAt(m).Value.strana);
                        Console.WriteLine("Дата рождения: " + Telbook.ElementAt(m).Value.Dateofbirthday);
                        Console.WriteLine("Организация: " + Telbook.ElementAt(m).Value.organizatia);
                        Console.WriteLine("Должность: " + Telbook.ElementAt(m).Value.dolzhnost);
                        Console.WriteLine("Прочие заметки: " + Telbook.ElementAt(m).Value.zapisi);
                        Console.WriteLine(" ");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        id2 = 0;
                        Console.Clear();
                        Console.WriteLine("Вот список всех ваших контактов в телефонной книге с краткой информацией о каждом:");
                        Console.WriteLine(" ");
                        foreach (Book p in Telbook.Values)
                        {
                            Console.WriteLine("Контакт " + id2 + " в телефонной книге");
                            Console.WriteLine("Фамилия: " + p.familiya);
                            Console.WriteLine("Имя: " + p.imya);
                            Console.WriteLine("Номер телефона: " + p.nomerTel);
                            Console.WriteLine(" ");
                            id2++;
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 6:
                        Console.Clear();
                        Console.WriteLine("До свидания!");
                        flag = false;
                        break;
                }
            }
            Console.ReadKey();
        }
    }
    class Book
    {
        public string familiya, imya, otchestvo, strana, Dateofbirthday, organizatia, dolzhnost, zapisi;
        public long nomerTel;
        public Book(string familiya, string imya, string otchestvo, long nomerTel, string strana, string Dateofbithday, string organizatia, string dolzhnost, string zapisi)
        {
            this.familiya = familiya;
            this.imya = imya;
            this.otchestvo = otchestvo;
            this.nomerTel = nomerTel;
            this.strana = strana;
            this.Dateofbirthday = Dateofbithday;
            this.organizatia = organizatia;
            this.dolzhnost = dolzhnost;
            this.zapisi = zapisi;
        }
    }
}